import flask
import os
from OCA.definitions import *

app = flask.Flask(__name__,  template_folder=TEMPLATING_PATH, static_folder=STATIC_PATH)
app.config["TEMPLATES_AUTO_RELOAD"] = True

@app.route("/")
def index():
    return flask.render_template("base.html", MODULES=MODULES)
    
@app.route("/modules/<directory>/<module>", methods=["GET"])
@app.route("/modules/<directory>/<module>/", methods=["GET"])
@app.route("/modules/<directory>/<module>/<path:path>", methods=["GET"])
def GET_modules(directory, module, path=""):
    args = path.split("/")
    if len(args) == 1 and args[0] == "": 
        del(args[0])
    return flask.render_template("base.html", MODULES=MODULES, **Module(directory, module, args).render())
    
@app.route("/modules/<directory>/<module>", methods=["POST"])
@app.route("/modules/<directory>/<module>/", methods=["POST"])
@app.route("/modules/<directory>/<module>/<path:path>", methods=["POST"])
def POST_modules(directory, module, path=""):
    args = path.split("/")
    if len(args) == 1 and args[0] == "": 
        del(args[0])
    return Module(directory, module, args).execute(request=flask.request)
    
def run():
    app.run(debug=True, host="0.0.0.0", port=80)

if __name__ == "__main__":
    run()