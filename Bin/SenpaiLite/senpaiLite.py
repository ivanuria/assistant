try:
    import senpai
    __package__ = "Senpai"
except ImportError:
    pass

import asyncio
import base64
import comtypes, comtypes.client
import datetime
import difflib
import json
import os
import re
import shelve
import string
import subprocess
import sys
import time
from .definitions import SENPAI, HISTORICY, CONFIG, LOCALPATH, SQL, PAPERWORK
from decimal import Decimal
from events import *
from threading import Thread, Lock, current_thread
from Mordor import TheOneRing, Council, FellowshipOfTheRing, AlreadyServing, Lembas, LembasList, FlyYouFools
from multiprocessing import Pipe
from adohelper import Where, sql
from zashel.utils import copy, paste

# New Events
CloseServiceEvent = Event("close_service")
OpenServiceEvent = Event("open_service")
NewHistoryEvent = Event("new_history")
CommitmentsLoadedEvent = Event("commitments_loaded")
ParsedPaperEvent = Event("parsed_paper")
NewCustomerEvent = Event("new_customer")
GotUsersEvent = Event("got_users")


re_cc_orange = re.compile(r"(1\.[0-9]{8})", re.IGNORECASE)
re_nif = re.compile(r"([a-z]?[0-9]{7,9}-?[a-z]{1})", re.IGNORECASE)
re_cif = re.compile(r"([a-z]{1}[0-9]{8,9})", re.IGNORECASE)
re_phone = re.compile(r"([6-9]{1}[0-9]{8})", re.IGNORECASE)
re_papers = {#"acuerdo a plazos": re.compile(r"\s(a\w+\s*(a)?\s*p\w+)", re.IGNORECASE),
             "aap": re.compile(r"\s(aap)\s"),
             "cortesia": re.compile(r"\s(c\w+\s*)", re.IGNORECASE),
             "ciclo": re.compile(r"\s(c\w+)", re.IGNORECASE),
             "condonacion": re.compile(r"\s(c\w+\s*)", re.IGNORECASE),
             "condonar": re.compile(r"\s(c\w+)", re.IGNORECASE),
             "cuenta": re.compile(r"\s(cu\w+)", re.IGNORECASE),
             "direccion": re.compile(r"\s(c\w+\s+[de]?\s*d\w+)", re.IGNORECASE),
             "duplicado": re.compile(r"\s(d\w+)", re.IGNORECASE),
             "infosms": re.compile(r"\sinfo\s*sms\s*\d+", re.IGNORECASE), # info sms
             #"cp": re.compile(r"\s(cp) ", re.IGNORECASE), # cp
             #"compromiso de pago": re.compile(r"(c\w+\s*(de)?\s*p\w+)", re.IGNORECASE),
             "localizar pago": re.compile(r"\s(l\w+\s*p\w+)", re.IGNORECASE),
             
             "buscar pago": re.compile(r"\s(b\w+\s*p\w+)", re.IGNORECASE),
             "acuerdo": re.compile(r"\s(a\w+)", re.IGNORECASE),
             "pago en": re.compile(r"\s(p\w+\s+[en]{2})", re.IGNORECASE),
             "anticipo": re.compile(r"\s(a\w+)", re.IGNORECASE),
             "anticipado": re.compile(r"\s(a\w+)", re.IGNORECASE),
             "mal aplicado": re.compile(r"\s(m\w+\s+a\w+)", re.IGNORECASE),
             "vendid": re.compile(r"\s(v\w+)", re.IGNORECASE),
             
             "contingencia": re.compile(r"\s(c\w+)"),
             "exceso": re.compile(r"\s(e\w+)"),
             
             "check impago": re.compile(r"\s(c\w+\s*i\w+)"),
             "desbloqueo": re.compile(r"\s(d\w+)"),
             "reconexiones": re.compile(r"\s(r\w+)"),
             "suspensiones": re.compile(r"\s(s\w+)"),
             "pedidos": re.compile(r"\s(p\w+)"),
             "dispositivo": re.compile(r"\s(d\w+)"),
             "localizacion": re.compile(r"\s(l\w+)", re.IGNORECASE),
             "compensacion": re.compile(r"\s(c\w+)"),
             "ajustes": re.compile(r"\s(a\w+)"),
             "quita": re.compile(r"\s(q\w+)"),
             }
re_importe = re.compile(r"\s(\d+[\.,]?\d*)\s*\u20ac?\s", re.IGNORECASE)
re_fecha = re.compile(r"\d{1,2}[/-]\d{1,2}[/-]?\d{0,4}", re.IGNORECASE)
re_hoy = re.compile(r"hoy", re.IGNORECASE)
re_ayer = re.compile(r"ayer", re.IGNORECASE)
re_manana = re.compile(r"mañana", re.IGNORECASE)
re_factura = re.compile(r"f[ract]{2,3}\.?:?\w*\s+[del]?\s*(\d{1,2}[/-]\d{1,2}[/-]?\d{0,4})", re.IGNORECASE)
re_banco = re.compile(r"bsch|santander|bbva?|bankia|cajero", re.IGNORECASE)
re_office = re.compile(r"of\w*\s+(\d{4})", re.IGNORECASE)
re_cents = re.compile(r"(\d+)\s+cent", re.IGNORECASE)
re_cc_mal_aplicado = re.compile(r"\sen\s+(1.\d{8})|\s(vendid)", re.IGNORECASE)
re_id_open = re.compile(r"\sid\s*\w*\s*(\d{5,10})", re.IGNORECASE)
re_para_el = re.compile(r"\sp\w*\s+[el]?\s*(\d{1,2}[/-]\d{1,2}[/-]?\d{0,4})", re.IGNORECASE)
re_id_cuenta = re.compile((r"\sid:?\s*(\d+)"), re.IGNORECASE)
re_incidencia = re.compile(r"\s(1-[0-9]{7,20})\s")
re_external_id = re.compile(r"\s([0-9]{6,12})\s")


#Exceptions
class uAgentError(Exception):
    """
    Base class to uAgent Errors
    """


class NotOpenAltitudeError(uAgentError):
    """
    Exception to raise in case altitude is not open
    """


PIPEIN, PIPEOUT = Pipe(False)
#uAgentHandler class
def uAgentHandler(pipe, pipe2=PIPEOUT):
    def uAgentHandler_wrapper(pipe):
        """
        Handler to events in pipe setted in uAgent
        :param pipe: pipe to receive data with
        :return: None
        """
        print(f"uAgentAPI PIPE Thread {current_thread()}*********************************")
        while True:
            try:
                event_name, data = pipe.recv()
            except (ConnectionRefusedError, ConnectionResetError):
                break
            else:
                print(f"En uAgent {event_name}")
                pipe2.send((event_name, data))
    return uAgentHandler_wrapper(pipe)


def FOOLShandler(): # Fly, you fools!
    print(current_thread())
    while True:
        try:
            event_name, data = PIPEIN.recv()
        except (ConnectionRefusedError, ConnectionResetError):
            print("Conexión rechazadaaaaa")
            break
        else:
            print(f"REGISTERING {event_name}")
            Gandalf.register_event("uAgent_"+event_name, raises=False)
            Gandalf.trigger_event("uAgent_"+event_name, *data)



def parse_value(value): # Do not repeat yourself. Parse values to friendly sql string
    """
    Parses a value to insert it in an SQL.
    :param value: Value to parse
    :return: str
    """
    if isinstance(value, (datetime.datetime, datetime.timedelta)):
        return value.strftime("%d/%m/%Y %H:%M:%S")
    elif isinstance(value, float):
        return str(value).replace(".", ",")
    elif isinstance(value, Decimal):
        return value.to_eng_string().replace(".", ",")
    else:
        return str(value)


class Record(dict):
    def __init__(self, row, data, cursor, back_correlations, keys):
        #self.cursor = cursor
        self.back_correlations = back_correlations
        self.correlations = dict([(self.back_correlations[key], key) for key in self.back_correlations])
        self.keys = [key.lower() for key in keys]
        #self.row = row
        super().__init__(data)
        #for item in self.keys:
        #    self[item.lower()] = cursor.Fields.Item(self.keys.index(item)).Value
        self.update(data)


    def __getitem__(self, item):
        if isinstance(item, str):
            item = item.lower()
        if item in self:
            return dict.__getitem__(self, item)


    def __repr__(self):
        for key in self.keys:
            dict.__setitem__(self, key, self[key])
        return super().__repr__()

    def __str__(self):
        for key in self.keys:
            dict.__setitem__(self, key, self[key])
        return super().__str__()


class Row:
    totales = 0
    class Iter:
        def __init__(self, iterator):
            self.iterator = iterator
        def __next__(self):
            print("next")
            index = -1
            while True:
                index += 1
                try:
                    yield self.iterator[index]
                except KeyError:
                    print("STOP")
                    raise StopIteration()

    def __init__(self, conn, cursor, back_correlations):
        self.conn = conn
        self.cursor = cursor.Clone()
        self.back_correlations = back_correlations
        self.correlations = dict([(self.back_correlations[key], key) for key in self.back_correlations])
        self.fields = self.cursor.Fields
        self.keys = list()
        for index in range(self.fields.Count):
            key = self.fields.Item(index).Name
            if key in self.back_correlations:
                self.keys.append(self.back_correlations[key])
            else:
                self.keys.append(key)
        Row.totales += 1

    def __del__(self):
        self.cursor.Close()
        self.conn.Close()
        Row.totales -= 1

    def __getitem__(self, item):
        try:
            if isinstance(item, int):
                cursor = self.cursor
                if item >= 0:
                    cursor.MoveFirst()
                else:
                    item += 1
                    cursor.MoveLast()
                try:
                    cursor.Move(item)
                except comtypes.COMError:
                    raise KeyError()
                fields = cursor.Fields
                #for x in range(item):
                #    self.cursor.MoveNext()
                if cursor.EOF is False and cursor.BOF is False:
                    return Record(self,
                                  dict(zip(self.keys, [fields.Item(index).Value for index, key in enumerate(self.keys)])),
                                  cursor,
                                  self.back_correlations,
                                  self.keys)
        except comtypes.COMError:
            pass
        raise IndexError(f"{item} not found")

    def __len__(self):
        cursor = self.conn.execute(f"select count(*) as total from ({self.cursor.Source})")
        return int(cursor[1].Fields.Item(0).Value)

    def append(self, data):
        cursor = self.cursor.Clone()
        fields = cursor.Fields
        assert all([key in self.keys for key in data])
        keys = list(data.keys())
        keys.sort()
        cursor.AddNew([self.correlations[key] for key in keys], [data[key] for key in keys]) # It may not have to be ordered
        return Record(dict(zip(self.keys, [fields.Item(index).Value for index, key in enumerate(self.keys)])),
                      cursor,
                      self.back_correlations,
                      self.keys)


class Paperwork:
    CHECKED = False
    def __init__(self, name, correlations=None):
        """
        Initializes Paperwork
        :param name: name of the Paperwork
        :param definitions: dict with definition of the class.
            {"table": table,
             "insert_fields": fields separated by ", ",
             "update_fields": fields separated by ", ",
             "insert_user_field": field for user,
             "status": field for status,
             "insert_date", field for date od insertion,
             "update_user_field": field for user who updates data,
             "update_data": field for updated data date,
             "conn_type": type of connection,
             "conn": connection string if conn_type == connection_string }
        :param correlations: dict with correlation of variables.
            {"phone": "TLF",
             "invoice_date": "FECHA FACTURA",
             ... }
        """
        self._initialized = False
        self._name = name
        self._definitions = SQL[name]
        self._definitions["path"] = SQL["DB_NAMES"][self._definitions["data_source"]]
        if self._definitions["data_source"] in SQL["DB_PASSWORDS"]:
            self._definitions["password"] = f'Jet OLEDB:Database Password={SQL["DB_PASSWORDS"][self._definitions["data_source"]]};'
        else:
            self._definitions["password"] = ""
        self._definitions["conn"] = f"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={self._definitions['path']};" \
                                    f"{self._definitions['password']}"
        self._correlations = correlations
        if not Paperwork.CHECKED: # Debugged. 
            while True:
                try:
                    comtypes.CoInitialize()
                    try:
                        conn = comtypes.client.CreateObject("ADODB.Connection")
                    except OSError as e:
                        print(e)
                        raise
                    conn.Mode = 3  # Read/Write
                    cursor = comtypes.client.CreateObject("ADODB.RecordSet")
                    definitions = SQL["BUG_REPORTING_PAPER"]
                    definitions["path"] = SQL["DB_NAMES"][definitions["data_source"]]
                    if definitions["data_source"] in SQL["DB_PASSWORDS"]:
                        definitions["password"] = f'Jet OLEDB:Database Password={SQL["DB_PASSWORDS"][definitions["data_source"]]};'
                    else:
                        definitions["password"] = ""
                    definitions["conn"] = f"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={definitions['path']};" \
                                          f"{definitions['password']}"
                    conn.Open(definitions["conn"])
                except (comtypes.COMError): #THIS
                    if os.path.exists(SQL["DB_NAMES"]["Developing"]) is True:
                        print("install")
                        p = subprocess.Popen(["msiexec", "/i", os.path.join(os.path.split(sys.executable)[0], "AceRedist.msi"), 
                                              "/quiet", "/norestart", "/qn"])
                        p.wait()
                    else:
                        Paperwork.CHECKED = True # Raise Window
                else:
                    Paperwork.CHECKED = True
                    break
                finally:
                    try:
                        conn.close()
                    except (UnboundLocalError, comtypes.COMError):
                        pass
        self.initialize()

    def initialize(self):
        fields = ["table", "insert_fields", "update_fields", "insert_user_field", "status", "insert_date",
                  "update_user_field", "update_date", "conn"]
        if self._initialized is False:
            self._definitions = SQL[self._name]
            self._definitions["path"] = SQL["DB_NAMES"][self._definitions["data_source"]]
            if self._definitions["data_source"] in SQL["DB_PASSWORDS"]:
                self._definitions["password"] = f'Jet OLEDB:Database Password={SQL["DB_PASSWORDS"][self._definitions["data_source"]]};'
            else:
                self._definitions["password"] = ""
            self._definitions["conn"] = f"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={self._definitions['path']};" \
                                        f"{self._definitions['password']}"
            assert all([item in self._definitions for item in fields])
            self._table = self._definitions["table"]
            if isinstance(self._definitions["insert_fields"], (list, tuple)):
                self._insert_fields = self._definitions["insert_fields"]
            elif isinstance(self._definitions["insert_fields"], str): # Legacy!
                self._insert_fields = [item.strip() for item in self._definitions["insert_fields"].split(",")]
            if isinstance(self._definitions["update_fields"], (list, tuple)):
                self._insert_fields = self._definitions["update_fields"]
            elif isinstance(self._definitions["update_fields"], str): # Legacy!
                self._update_fields = [item.strip() for item in self._definitions["update_fields"].split(",")]
            self._insert_user_field = self._definitions["insert_user_field"]
            self._status = self._definitions["status"]
            self._insert_date = self._definitions["insert_date"]
            self._update_user_field = self._definitions["update_user_field"]
            self._update_date = self._definitions["update_date"]
            self._conn = self._definitions["conn"]
            if self._correlations is not None and not "correlations" in self._definitions:
                pass
                # self._correlations = correlations
            elif "correlations" in self._definitions:
                if isinstance(self._definitions["correlations"], str): # Legacy!
                    self._correlations = eval(self._definitions["correlations"])
                else:
                    self._correlations = self._definitions["correlations"]
            else:
                self._correlations = dict(zip(self._insert_fields+self._update_fields,
                                              self._insert_fields+self._update_fields))
            self._correlations.update({"id": "Id",
                                       "insert_user": self._insert_user_field,
                                       "insert_date": self._insert_date,
                                       "status": self._status,
                                       "update_user": self._update_user_field,
                                       "update_date": self._update_date})
            self._correlations = dict([(key.lower(), self._correlations[key]) for key in self._correlations])
            self._back_correlations = dict([(self._correlations[key], key) for key in self._correlations])

    @property
    def conn(self):
        return self._conn

    @property
    def table(self):
        return self._table

    @property
    def update_user(self):
        return self._update_user_field

    def sqlexecute(self, template, *, values=None, fields=None, where=None, order=None, limit=0):
        """
        Executes sql with given data
        :param template: template to use between "SELECT", "INSERT", "UPDATE", "DELETE"
        :param values: list of values to set
        :param fields: list of fields to get/set
        :param where: list of tuples with (field, operator, value)
        :return: list of dicts if template == "SELECT", None by default
        """
        if self._initialized is False:
            self.initialize()
        if values is None:
            values = list()
        if fields is None:
            fields = "*"
        if where is None:
            where = list()
        old_values = values
        group = list()
        do_group = False
        values = list()
        for value in old_values:
            values.append(parse_value(value))
        del(old_values)
        for index, value in enumerate(values):
            if value is not None and value != "None" and value != "":
                values[index] = "'"+str(value)+"'"
            else:
                values[index] = "Null"
        #values = ["'"+value+"'" for value in values]
        if values is not None and fields is not None:
            fields_values = ", ".join([" = ".join(item) for item in zip(fields, values)])
        else:
            fields_values = ""
        values = ", ".join(values)
        for field in fields:
            if "(" in field:
                do_group = True
            else:
                group.append("["+field+"]")
        if fields != "*":
            fields = ", ".join(["(" not in field and "["+field+"]" or field for field in fields])
        table = "["+self._table+"]"
        if not isinstance(where, Where):
            old_where = where
            where = Where()
            for item in old_where:
                one = self._correlations[item[0]] # Key
                two = item[1] # Operator
                three = item[2] #This is the value
                where.And(one, two, three)
            del(old_where)
        else:
            def check_where(where):
                for index in range(len(where)): #Do not iterate and change it
                    if isinstance(where[index], Where):
                        where[index] = check_where(where[index])
                    else:
                        if isinstance(where[index], Where.Clause):
                            key = where[index][0].lower()
                            if key in self._correlations:
                                where[index][0] = Where.Clause.Operator(self._correlations[key])
                            elif key in [key.lower() for key in self._correlations.keys()]:
                                where[index][0] = Where.Clause.Operator(self._correlations[key.lower()])
                            elif key in self._back_correlations or key in [key.lower() for key in self._back_correlations.keys()]:
                                pass
                            else:
                                raise IndexError(f"{key} not in {table}")
                return where
            where = check_where(where)
        #where = " and ".join([" ".join(item) for item in where])
        where = where.sql
        sql_clause = eval("f\"" + SQL.__getattr__("GENERIC_"+template)+"\"")
        if order is not None:
            sql_clause += " order by "+", ".join(order)
        elif do_group is True and len(group) > 0:
            sql_clause += " group by "+", ".join(list(set(group)))
        if limit is not None and limit > 0 and sql_clause.startswith("select "):
            sql_clause = f"select top {limit} " + sql_clause[len("select "):]
        return self.sql_direct(sql_clause, template=template)

    def sql_direct(self, sql, *, template="SELECT"):
        if self._initialized is False:
            self.initialize()
        comtypes.CoInitialize()
        conn = comtypes.client.CreateObject("ADODB.Connection")
        conn.Mode = 3 # Read/Write
        cursor = comtypes.client.CreateObject("ADODB.RecordSet")
        conn.Open(self._conn)
        # Verificamos tipo de cursor que abrimos.
        # https://docs.microsoft.com/es-es/sql/ado/reference/ado-api/cursortypeenum
        # cursor = conn.execute(sql_clause)
        cursor.CursorType = 1 # Let's Try KeySet
        cursor.LockType = 2 # Pessimistic
        cursor.Open(sql, conn)
        if template == "SELECT":
            return Row(conn, cursor, self._back_correlations)
        try:
            cursor.Close()
        except comtypes.COMError:
            pass
        conn.Close()
        return None

    def get(self, filter=None, *, fields=None, order=["Id asc"], limit=0):
        """
        Gets data and return list of dictionary
        :param filter: list of tuples with (field, operator, value) or Where instance
        :return: list of dicts with data
        """
        if self._initialized is False:
            self.initialize()
        return self.sqlexecute("SELECT", where=filter, fields=fields, order=order, limit=limit)

    def get_insert_fields(self):
        """
        Gets list of insert_fields
        :return: list
        """
        if self._initialized is False: # initialize if is not (to solve bunny on paperwork)
            self.initialize()
        final = ["insert_user", "insert_date", "status"]
        for item in self._insert_fields:
            if item in self._back_correlations:
                final.append(self._back_correlations[item])
        return final

    def get_update_fields(self):
        """
        Gets list of update_fields
        :return: list
        """
        if self._initialized is False: # initialize if is not (to solve bunny on paperwork)
            self.initialize()
        final = ["status"]
        for item in self._update_fields:
            if item in self._back_correlations:
                final.append(self._back_correlations[item])
        return final

    def get_all_fields(self):
        insert_fields = self.get_insert_fields()
        del(insert_fields[insert_fields.index("status")])
        update_fields = self.get_update_fields()
        del(update_fields[update_fields.index("status")])
        if "update_comment" in update_fields:
            del(update_fields[update_fields.index("update_comment")])
        for field in update_fields:
            if field in insert_fields:
                del(insert_fields[insert_fields.index(field)])
        update_fields.append("update_comment")
        update_fields.append("status")
        return ["id"] + insert_fields + update_fields

    def post(self, data):
        """
        Inserts new data in table
        :param data: data in form of dictionary
        :return: None
        """
        if self._initialized is False:
            self.initialize()     
        print(list(self._correlations.keys()))
        assert all([key in self._correlations for key in data])
        mess = [(self._correlations[key], data[key]) for key in data]
        fields = list()
        values = list()
        for item in mess:
            fields.append(item[0])
            values.append(item[1])
        self.sqlexecute("INSERT", fields=fields, values=values)

    def put(self, filter, data):
        """
        Inserts given filter with given data
        :param filter: list of tuples with (field, operator, value) or Where instance
        :param data: data in form of dictionary
        :return: None
        """
        if self._initialized is False:
            self.initialize()
        assert all([key in self._correlations for key in data])
        mess = [(self._correlations[key], data[key]) for key in data]
        fields = list()
        values = list()
        for item in mess:
            fields.append(item[0])
            values.append(item[1])
        self.sqlexecute("UPDATE", fields=fields, values=values, where=filter)

    def delete(self, filter):
        """
        Deletes rows wich validates given filter
        :param filter: list of tuples with (field, operator, value) or Where instance
        :return: None
        """
        if self._initialized is False:
            self.initialize()
        return self.sqlexecute("DELETE", where=filter)


def get_connections():
    return Row.totales


Papers = {}
for item in SQL:
    if item.endswith("_PAPER"):
        Papers[item] = Paperwork(item)
DEBUG = Paperwork("BUG_REPORTING_PAPER")



#The freaking API
class SenpaiAPI(FirstLightOnTheFifthDay):
    """
    API for senpai to implement in forward GUI
    """
    GOOGLE = SENPAI # Google api to spreadsheets and drive
    CONFIG = CONFIG # Configuration File
    #PAPERBACK = PAPERBACK # Paperback file

    def __init__(self):
        """
        Initializes the API
        """
        #print("LOADING SenpaiAPI")
        #TheOneRing.__init__(self, "Senpai", manager_port=5506)
        FirstLightOnTheFifthDay.__init__(self)
        self.Gandalf_Status = True
        self._customers = {}
        Thread(target=FOOLShandler, daemon=True).start()
        # Variables
        self._commitments = {} # Commitments list to show
        self._open_campaigns = [] # Campaigns open
        self._open_services = []
        self._phone_history = []
        self._customer_history = []
        self._custcode_history = []
        self.username = os.environ["USERNAME"] # Te pongo el usuario de Windows!!!
        self.commitments_helper = None
        self.lock = Lock()
        self.altitude_lock = Lock()
        print(f"SenpaiAPI running in {current_thread()}")

    @property
    def commitments(self):
        """
        Returns self._commitments.
        """
        return self._commitments

    @property
    def customers(self):
        """
        Returns dictionary of customers by customer_id.
        """
        return self._customers

    @property
    def open_campaigns(self):
        """
        Returns list of open campaigns in uAgent.
        """
        return self._open_campaigns

    @property
    def customer_history(self):
        """
        Resturns Persistency instance of customer history.
        """
        return self._customer_history

    @property
    def custcode_history(self):
        """
        Resturns Persistency instance of customercode history.
        """
        return self._custcode_history
        

    @property
    def phone_history(self):
        """
        Returns Persistency instance of phone history.
        """
        return self._phone_history

    def exit(self):
        """
        It necesarily might exit cleanly.
        :return: None.
        """
        try:
            pass
        except (comtypes.COMError, ConnectionRefusedError):
            pass
            #self._phone_history.save_and_exit()
            #self._customer_history.save_and_exit()



    def get_session_data(self, session_id):
        #TODO
        pass

    def get_config(self):
        """
        Getter to CONFIG to use with managers
        :return: API.CONFIG
        """
        return self.CONFIG

    def get_google(self):
        """
        Getter to GOOGLE to use with managers
        :return: API.GOOGLE
        """
        return self.GOOGLE

    def get_my_special_actions(self): #TODO
        """
        Returns every Special Action and what it's done with it
        """
        if any("RecobsOrange" in campaign for campaign in self.open_campaigns):
            today = datetime.datetime.today()
            today = datetime.datetime(today.year, today.month, today.day)
            where = [["insert_user", "=", self.username], ["insert_date", ">=", today-datetime.timedelta(days=30)]]
            data = Papers["BO_ORANGE_INFOSMS_PAPER"].get(where)
            return data
        else:
            pass

    def get_my_special_actions_performance(self): #TODO
        """
        Returns every Special Action and what it's done with it
        """
        if any("RecobsOrange" in campaign for campaign in self.open_campaigns):
            today = datetime.datetime.today()
            this_month = today.month
            this_year = today.year
            last_month = this_month > 0 and this_month - 1 or 12
            last_year = last_month == 12 and this_year - 1 or this_year
            next_month = this_month < 12 and this_month + 1 or 1
            next_year = next_month == 1 and this_year + 1 or this_year

            initial = datetime.datetime(last_year, last_month, 1).strftime("%Y-%m-%d")
            middle = datetime.datetime(this_year, this_month, 1).strftime("%Y-%m-%d")
            end = datetime.datetime(next_year, next_month, 1).strftime("%Y-%m-%d")

            sql = "select SEGMENTO as segmento, sum(IMPORTE_PAGADO) as amount from InfoSMS where " \
                  "FECHA_DE_PAGO >= #{}# and FECHA_DE_PAGO < #{}# and TSFCP = '{}' and COMPUTA = 'OK' group by SEGMENTO"
            
            data = {"last": Papers["BO_ORANGE_INFOSMS_PAPER"].sql_direct(sql.format(initial, middle, self.username)),
                    "current": Papers["BO_ORANGE_INFOSMS_PAPER"].sql_direct(sql.format(middle, end, self.username))}

            return data
        else:
            pass            

    def get_paperback(self):
        """
        Getter to PAPERBACK to use with managers
        :return: API.PAPERBACK
        """
        return self.PAPERBACK



    def get_users(self, *, fullname=None, usr_name=None, login=None, only_loggedin=False):

        import getpass
        data = getpass.getuser()
        print(f"Encontrados {len(data)} usuarios con esos datos")
        GotUsersEvent(data)

    def historicy_session(self, session_id, session_data):
        #TODO
        pass

    def initialize(self):
        """
        Initializes SenpaiAPI.
        :return: None
        """
        #print("INITIALIZE SENPAI")
        #self._phone_history = Persistency("phone_history", ["number", "time"])
        #self._customer_history = Persistency("customer_history", ["number", "customer_id", "external_id"])


    def login(self, username):
        """
        Searches for an open instance of altitude and attaches it.
        #TODO: in case of shitty altitude shutdown, reinitilize the uAgent app
        :param username: username to altitude
        :return: None
        """
        self.username = username
        NewCustomerEvent({"insert_user": username}) # Chapuza

    def is_logged(self):
        '''
        Return the name of the Altitude logged agent
        :return: Login
        '''
        login = self.username
        return login

    def save_paperback(self, paperback, data): # Deprecated
        """
        Saves data in paperback.
        :param paperback: Type of data to be saved.
        :param data: Data to save.
        :return: None.
        """
        keys = PAPERBACK[paperback]
        final = dict()
        for key in keys:
            if key in data:
                to_save = data[key]
            else:
                to_save = ""
            final[key] = to_save
            final["user"] = os.environ["USERNAME"]
            final["date"] = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        files = list(filter(lambda x: paperback in x, self.GOOGLE.files))
        files.sort()
        files.reverse()
        new = None
        if len(files) > 1:
            while True:
                try:
                    spreadsheet = self.GOOGLE.spreadsheet_open(files[1]) # Files[0] May be a template
                except FileNotFoundError:
                    time.sleep(1)
                    continue
                else:
                    break
        else:
            new = 0
        if new is None and spreadsheet.get_total_cells() > CONFIG.SPREADSHEET_TOTAL_CELLS:
            new = int(files[1].split("_")[-1]) + 1
        if new is not None:
            name = "_".join((paperback, "0"*(5-len(str(new)))+str(new)))
            self.GOOGLE.file_copy(files[0], name)
            time.sleep(1.5)
        while True:
            try:
                spreadsheet = self.GOOGLE.spreadsheet_open(name)  # Files[0] May be a template
            except FileNotFoundError:
                time.sleep(1)
                continue
            else:
                break
        spreadsheet.append_row([str(base64.b85encode(bytearray(json.dumps(final), "cp1252"))), "PENDIENTE"])

    # Events
    def _event_clipboard_data(self, data): #TODO: Make order in this shit
        """
        Gets data from clipboard event.
        :param data: Data saved in clipboard.
        :return: None.
        """
        print("Gandalf is sniffing your clipboard. ^^")
        now = datetime.datetime.now()
        print(now)
        service = ""
        current_year = datetime.datetime.now().year
        data = data.replace("*", " ") #Quitamos los asteriscos
        if len(self.open_campaigns)>0: # checks if is logged before search in uAgent
            for camp in self.open_campaigns: #debug purposse only
                if "RecobsOrange" in camp:
                    service = "ORANGE"
                elif "jazz" in camp.lower():
                    service = "JAZZTEL"
                elif "JZT" in camp:
                    service = "JAZZTEL"
            print(service)
            comment = data
            segment = camp
            if not segment:
                segment = "CAMPAÑA NO ABIERTA"
            try: #do not repeat yourself
                data = " " + data.lower() + " "
                cc_orange = None
                if service == "ORANGE":
                    cc_orange = list(set(re_cc_orange.findall(data)))
                nif = list(set(re_nif.findall(data)))
                cif = list(set(re_cif.findall(data)))
                phone = list(set(re_phone.findall(data)))
                idcuenta = re_id_cuenta.findall(data)
                external_id = list(set(re_external_id.findall(data)))
                # Here we set "mal aplicado" cc... I don't know whether it will fail nor how
                mal_aplicado = list(re_cc_mal_aplicado.findall(data))
                print(f"MAL APLICADO: {mal_aplicado}")
                if len(mal_aplicado) > 0: # There is an "OR" in the RE
                    mal_aplicado = list(mal_aplicado[0])
                for item in mal_aplicado:
                    if "v" in item:
                        mal_aplicado = "recibo vendido"
                if mal_aplicado != "recibo vendido":
                    for item in mal_aplicado:
                        if cc_orange and item in cc_orange:
                            del(cc_orange[cc_orange.index(item)])
                            mal_aplicado = item
                            break
                # "Mal aplicado" till here
                print("IMPORTES")
                importes = [x.replace(".", ",").replace(" ", "") for x in re_importe.findall(data)]
                if service == "ORANGE":
                    for cc in cc_orange:
                        print("CC")
                        cc = cc.replace(".", ",")
                        if cc in importes:
                            del (importes[importes.index(cc)])
                    if len(mal_aplicado) > 0:  #now its works!
                        print("MAL APLICADO")
                        if mal_aplicado.replace(".", ",") in importes:
                            del (importes[importes.index(mal_aplicado.replace(".", ","))])
                            print(f"CC Orange: {cc_orange}")
                            print(f"POST IMPORTES: {importes}")
                elif service == "JAZZTEL":
                    for index, dni in enumerate(nif):
                        print("DNI")
                        dni = dni.replace("-","")
                        nif[index] = dni[0:8] + "-" + dni[8:9] # TODO: CIFS
                        if dni[0:8] in importes:
                            del(importes[importes.index(dni[0:8])])
                if any(["," in importe for importe in importes]):
                    importes = list(filter(lambda x: "," in x, importes))
                print(f"Importes: {importes}")
                print(f"DNIs: {nif}")
                print(f"CIFs: {cif}")
                print(f"Teléfonos: {phone}")
                this = dict()

                if service == "ORANGE":
                    cust_id = cc_orange #using cust_id to make the same for orange and jazztel
                elif service == "JAZZTEL":
                    cust_id = nif
                    if not cust_id:
                        cust_id = external_id
                print(f"CustID: {cust_id}")
                for id_ in cust_id:
                    this = {"number": phone,
                            "from_altitude": False}
                    if service == "ORANGE":
                        this.update({"customer_id": id_,
                                     "external_id": id_,
                                     "nif": cif and cif or nif,
                                     "service": "ORANGE"})
                    elif service == "JAZZTEL":
                        this.update({"number": phone,
                                     "customer_id": id_,
                                     "external_id": id_,
                                     "service": "JAZZTEL"})
                    if id not in self.custcode_history:  # check if is duplicated
                        with self.lock:
                            self.customer_history.append(this)
                            self.custcode_history.append(id_)

                        # Let's raise this thing:
                        # NewCustomerEvent(this) # _event_new_customer_event
                print(f"customer historicy {self.customer_history}")

                if len(cust_id) > 0:
                    NewHistoryEvent()
                else:
                    this.update({"from_altitude": False})
                # Paperworks
                factura = re_factura.findall(data) # Data is modified
                office = re_office.findall(data)
                idopen = re_id_open.findall(data)
                para_el = re_para_el.findall(data)
                fechas = re_fecha.findall(data)
                fechas = ["/".join([len(d)==1 and "0"+str(d) or d for d in item.replace("-", "/").split("/")]) for item in fechas]
                factura = ["/".join([len(d)==1 and "0"+str(d) or d for d in item.replace("-", "/").split("/")]) for item in factura]
                para_el = ["/".join([len(d)==1 and "0"+str(d) or d for d in item.replace("-", "/").split("/")]) for item in para_el]
                
                fechas = [len(item)==5 and f"{item}/{current_year}" or item for item in fechas]
                factura = [len(item)==5 and f"{item}/{current_year}" or item for item in factura]
                para_el = [len(item)==5 and f"{item}/{current_year}" or item for item in para_el]

                commitment = re_incidencia.findall(data)
            
                for para in para_el:
                    if para in factura:
                        del(factura[factura.index(para)])
                for fact in factura:
                    if fact in fechas:
                        del(fechas[factura.index(fact)])
                if isinstance(idopen, (list, tuple)) and idopen:
                    idopen = max(idopen)
                papers = dict()
                for p in string.punctuation:
                    data = data.replace(p, f" {p} ")
                for paper in re_papers:
                    item = re_papers[paper].findall(' ' + data)

                    def normalize(d):
                        """
                        Normalizes data in this place
                        :param d: list with data
                        :return: list
                        """
                        final = list()
                        for item in d:
                            if not isinstance(item, (tuple, list)):
                                final.append(item)
                            else:
                                final.extend(normalize(item))
                        return final

                    item = normalize(item)
                    #print(f"{paper}: {item}")
                    d = difflib.get_close_matches(paper, item, cutoff=0.8)
                    #print(f"Difflib: {d}")
                    if len(d) > 0:
                        papers[paper] = d
                for cuenta in idcuenta:
                    if cuenta in importes:
                        del(importes[importes.index(cuenta)])
                # today = len(re_hoy.findall(data)) > 0
                yesterday = len(re_ayer.findall(data)) > 0
                tomorrow = len(re_manana.findall(data)) > 0
                days = yesterday and -1 or tomorrow and 1 or 0
                if len(fechas) > 0:
                    payment_date = fechas[0]
                else:
                    payment_date = (datetime.datetime.today() + datetime.timedelta(days=days)).strftime("%d/%m/%Y")
                if any(["\u20ac" in importe for importe in importes]):
                    importes = list(filter(lambda x: "\u20ac" in x, importes))
                if len(factura) == 0 and len(fechas) > 0:
                    factura = fechas
                aditional = {"amount": [importe.replace("\u20ac", "") for importe in importes if isinstance(importe, str)],
                             "fechas": [x.replace("-", "/") for x in fechas if isinstance(x, str)],
                             "payment_date": payment_date,
                             "invoice_date": factura,
                             "entity": re_banco.findall(data),
                             "office": office,
                             "nif": nif and nif or idcuenta,
                             "phone": phone,
                             "cif": cif,
                             "insert_user": self.username,
                             "comment": comment,
                             "segment": segment,
                             "cc_applied_in": mal_aplicado,
                             "payment_id": idopen,
                             "account_id": idcuenta,
                             "commitment": commitment}
                cents = re_cents.findall(data)
                if len(cents) > 0:
                    aditional["amount"] = [str(int(cent)/100).replace(".", ",") for cent in cents]
                if service == "ORANGE":
                    aditional.update({"cc": cust_id,
                                      "service": "RecobsOrange"})
                elif service == "JAZZTEL":
                    aditional.update({"cc": cust_id,
                                      "service": "JAZZTEL_COBROS"})
                    if idcuenta:
                        aditional["external_id"] = idcuenta
                final = dict(aditional)
                final.update(this)
                final.update({"paperwork": list(papers.keys())})
                #print(f"FINAL!!! {final}")
                if any(final[item] for item in final):
                    for item in final:
                        if item != "paperwork" and isinstance(final[item], (tuple, list)) and len(final[item]) > 0:
                            final[item] = final[item][0]
                        elif isinstance(final[item], (tuple, list)) and len(final[item]) == 0:
                            final[item] = str()
                    #print(f"FINAL!!! {final}")
                    NewCustomerEvent(final)  # Raises this shit
                    if len(final["paperwork"]) > 0:
                        #print(final["paperwork"])
                        if self.Gandalf_Status is True:
                            #ParsedPaperEvent(final)
                            pass
                        else:
                            print("Gandalf is disabled")
            except TypeError:
                print("Clipboard content not usefull")
        nnow = datetime.datetime.now()
        print(nnow)
        #print(f"Total: {(nnow-now).seconds} segundos")
        
    def _event_open_servide(self, service):
        print(f"from API {service}")

    def _event_commitments_loaded(self, commitments):
        print("Commitments gotten")


