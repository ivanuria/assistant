import os
import pythoncom
from Mordor import Council
from MordorManager import get_api
from zashel.gapi import SCOPE
from zashel.transcom import TranscomAPI


SCOPES = [SCOPE.DRIVE, SCOPE.SPREADSHEETS]



class Locale(dict):
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            return item
            

get_senpai = get_api
get_historicy = get_api

SENPAI = None
HISTORICY = None
PATH = os.path.join(os.environ["APPDATA"], "Senpai")
REMOTE_PATH = r"//172.19.64.20/cobros/O.B.O/Senpai/Config"
if not os.path.exists(PATH):
    os.makedirs(PATH)
CONFIG = Council(None, name="Config", path=PATH, remote_path=REMOTE_PATH)
PAPERWORK = Council(None, name="Paperwork", path=PATH, remote_path=REMOTE_PATH)
SQL = Council(None, name="Sql", path=PATH, remote_path=REMOTE_PATH)
LOCALPATH = os.path.join(os.environ["LOCALAPPDATA"], "senpai")
DEBUG_FLAG = True
LOCALE = Locale(CONFIG.LOCALE)


#ORANGE_PAPERS = PAPERWORK.ORANGE_PAPERS
#JAZZTEL_PAPERS = PAPERWORK.JAZZTEL_PAPERS


PROGRAMDATA = os.path.join(os.environ["PROGRAMDATA"], "Senpai")
if not os.path.exists(PROGRAMDATA):
    os.makedirs(PROGRAMDATA)
