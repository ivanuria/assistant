"""
Sorry for the not OI programming
"""
import gc
import os
import win32api
from comtypes import COMError
from tkutils import Window, Treeview
from tkinter import Menu, filedialog, StringVar, Entry, Label, Button
from tkinter import BOTH, X, Y
from tkinter.ttk import Notebook
from tkinter.tix import Frame
from tkadolink import ADODB, ADOTree, Tk, mainloop
from functools import partial


TITLE = "DB Substitute"
DATABASES = dict()
FRAMES = dict()

ROOT = Tk()
ROOT.title(TITLE)


def do_and_close(caller, window, event=None):
    caller()
    window.destroy()


def password_dialog(master, *, title="Contraseña requerida", text="Por favor, ingresa la contraseña"):
    var = [False] # Chapucilla funcional
    password = StringVar()
    dialog = Window(master, title=title, width=400, height=180)
    Label(dialog, text=text).pack(padx=50, pady=10)
    entry = Entry(dialog, textvariable=password, show="*")
    entry.pack(pady=10)
    entry.bind('<Return>', partial(do_and_close, lambda var=var: var.append(True), dialog))
    button_frame = Frame(dialog)
    button_frame.pack(pady=10)
    Button(button_frame, text="Abrir", command=partial(do_and_close, lambda var=var: var.append(True), dialog)).grid(column=1, row=1)
    Button(button_frame, text="Cerrar", command=partial(do_and_close, dialog.destroy)).grid(column=2, row=1)
    master.grab_set()
    master.wait_window(dialog)
    if any([item is True for item in var]):
        return password.get()
    else:
        return None
    gc.collect()


def ask_open(root):
    """
    Pide qué base de datos abrir
    """
    filename = filedialog.askopenfile(filetypes=[("Bases de Datos Access 97-2003", ".mdb")]).name
    do_open(root, filename)
        

def ask_password_and_open(root, filename, text=None):
    kwargs = dict()
    if text is not None:
        kwargs["text"] = text
    password = password_dialog(root, **kwargs)
    if password is not None:
        do_open(root, filename, password)


def show_table_context_menu(filename, event):
    x, y = event.x_root, event.y_root
    notebook = FRAMES["central"]["notebook"]
    tab = notebook.index("current")
    table = notebook.tab(tab, "text")
    print(table)
    menu = Menu(ROOT, tearoff=0)
    menu.add_command(label="Cerrar", command=partial(close_table, filename, table))
    try:
        menu.tk_popup(x, y, 0)
    finally:
        menu.grab_release()
    gc.collect()


def do_open(root, filename, password=None):
    print(f"password {type(password)}")
    try:
        kwargs = dict()
        if password is not None:
            kwargs["password"] = password
        db = ADODB(filename, **kwargs)
        print("Opening")
        DATABASES[filename] = {"db": db, "filename": filename, "kwargs": kwargs}
        del(DATABASES[filename]["db"])
        gc.collect()
        open_database(filename)
    except (COMError) as e:
        description = e.details[0]
        if any([item in description for item in (" contrase", " pasword ")]):
            if password is None:
                ask_password_and_open(root, filename)
            else:
                ask_password_and_open(root, filename, description)
        else:
            win32api.MessageBox(0, description)
    gc.collect()
        

def open_database(filename):
    path, _file = os.path.split(filename)
    name, ext = os.path.splitext(_file)
    if "lateral" not in FRAMES:
        FRAMES["lateral"] = dict()
        FRAMES["lateral"]["frame"] = Frame()
        FRAMES["lateral"]["frame"].grid(column=1, row=1)
        FRAMES["lateral"]["tree"] =  Treeview(FRAMES["lateral"]["frame"], height=35, columns=["name"], locale={"name": name},
                                              options={"copy_on_click": False, "filterable": False, "sortable": False})
    if "central" not in FRAMES:
        FRAMES["central"] = dict()
        FRAMES["central"]["frame"] = Frame()
        FRAMES["central"]["frame"].grid(column=2, row=1)
        FRAMES["central"]["notebook"] = Notebook(FRAMES["central"]["frame"], width=900)
        FRAMES["central"]["notebook"].pack(padx=5, pady=5, fill=BOTH)
        FRAMES["central"]["tabs"] = dict()
        FRAMES["central"]["trees"] = dict()
        FRAMES["central"]["label"] = Label(FRAMES["central"]["frame"], text="", width=100)
        FRAMES["central"]["label"].pack(fill=X)
    lateral = FRAMES["lateral"]["frame"]
    central =  FRAMES["central"]["frame"]
    tree = FRAMES["lateral"]["tree"]
    tree.column("#0", width=5)
    tree.column("name", width=150)
    tree.bind("<Double-1>", lambda event, filename=filename, tree=tree: open_table(filename, tree.item(tree.identify_row(event.y))["values"][0]))
    tree.tag_configure("node", background="gray50", foreground="white")
    tree.tag_configure("item", background="white")  
    tree.pack()
    FRAMES["central"]["notebook"].bind("<ButtonRelease-3>", partial(show_table_context_menu, filename))
    FRAMES["lateral"]["tree"] = tree
    update_lateral(filename)
    gc.collect()


def open_table(filename, table):
    db = ADODB(filename, **DATABASES[filename]["kwargs"])
    if not table in DATABASES[filename]:
        try:
            DATABASES[filename][table] = db[table]
        except COMError as e:
            description = e.details[0]
            win32api.MessageBox(0, description)
            return
    if not table in FRAMES["central"]["tabs"]:
        FRAMES["central"]["tabs"][table] = Frame(FRAMES["central"]["notebook"])
    if not table in FRAMES["central"]["trees"]:
        FRAMES["central"]["trees"][table] = ADOTree(FRAMES["central"]["tabs"][table], DATABASES[filename][table], height=33)
        FRAMES["central"]["trees"][table].pack()
    FRAMES["central"]["notebook"].add(FRAMES["central"]["tabs"][table], text=table)
    del(db)
    gc.collect()


def close_table(filename, table):
    if table in FRAMES["central"]["tabs"]:    
        FRAMES["central"]["notebook"].forget(FRAMES["central"]["tabs"][table])
        del(FRAMES["central"]["tabs"][table])
    if table in FRAMES["central"]["trees"]:
        FRAMES["central"]["trees"][table].destroy()
        del(FRAMES["central"]["trees"][table])
    if filename in DATABASES and table in DATABASES[filename]:
        DATABASES[filename][table]._table.disconnect()
        del(DATABASES[filename][table])
    gc.collect()


def update_lateral(filename):
    tree = FRAMES["lateral"]["tree"]
    for item in tree.get_children():
        tree.delete(item)
    if filename in DATABASES and "db" in DATABASES[filename]:
        db = DATABASES[filename]["db"]
    else:
        db = ADODB(filename, **DATABASES[filename]["kwargs"])
    tables, views = db.tables, db.views
    if len(tables) > 0:
        tables.sort()
        tabletag = tree.insert("", "end", values=["Tablas"], tags=["node"], open=True)
        for table in tables:
            tree.insert(tabletag, "end", values=[table], tags=["item"])
    if len(views) > 0:
        views.sort()
        viewtag = tree.insert("", "end", values=["Consultas"], tags=["node"], open=True)
        for view in views:
            tree.insert(viewtag, "end", values=[view], tags=["item"])
    del(db)
    gc.collect()
    

def main():
    """
    Ejecutamos
    """
    global DATABASES, FRAMES
    # MenuBar
    menubar = Menu(ROOT)
    file_options = Menu(menubar, tearoff=0)
    file_options.add_command(label="Abrir...", command=partial(ask_open, ROOT))
    menubar.add_cascade(label="Archivo", menu=file_options)
    ROOT.config(menu = menubar)
    mainloop()
    del(DATABASES)
    del(FRAMES)
    gc.collect()

main()
